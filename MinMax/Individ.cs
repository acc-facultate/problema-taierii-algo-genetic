﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Taiere2D
{
    class Individ
    {
        public int[,] Cromozom;
        // Cromozomul e de forma matrice:
        // prima linie - tipul de piesa
        // a doua linie - nr de exemplar
        // a treia linie - Zi
        // a patra linie - Xi
        // a cincea linie - Yi
        public Tuple<int,int> FitnessUnfitness { get; set; }
        //Primul element din tuplu e fitness, al doilea unfitness
        public Individ(int[,] matriceCromozom)
        {
            Cromozom = matriceCromozom;
        }
        public Individ(List<Piesa> programPieces, Placa cuttingSpace)
        {
            int noPieces = 0;
            for (int i = 0; i < programPieces.Count; i++)
            {
                noPieces += programPieces[i].NumarExemplare;
            }
            Cromozom = new int[5, noPieces];
            var randomNumberGenerator = new Random();

            int currentPiece = 0;
            int currentCopyOfPiece = programPieces[currentPiece].NumarExemplare;

            for (int i = 0; i < noPieces; i++)
            {
                Cromozom[0, i] = currentPiece;
                Cromozom[1, i] = currentCopyOfPiece;
                Cromozom[2, i] = 1;
                if (programPieces[Cromozom[0, i]].Lungime == cuttingSpace.Lungime)
                    Cromozom[3, i] = cuttingSpace.Lungime / 2;
                else
                    Cromozom[3, i] = randomNumberGenerator.Next(programPieces[0].Lungime / 2,
                        cuttingSpace.Lungime - programPieces[0].Lungime / 2);
                if(programPieces[Cromozom[0, i]].Latime == cuttingSpace.Latime)
                    Cromozom[4, i] = cuttingSpace.Latime / 2;
                else
                    Cromozom[4, i] = randomNumberGenerator.Next(programPieces[0].Latime / 2,
                        cuttingSpace.Latime - programPieces[0].Latime / 2);

                currentCopyOfPiece--;
                if (currentCopyOfPiece == 0 && i!= noPieces-1)
                {
                    currentPiece++;
                    currentCopyOfPiece = programPieces[currentPiece].NumarExemplare;

                }
            }
            FitnessUnfitness = CalculateFitnessUnfitness(programPieces);
        }
        public Tuple<int,int> CalculateFitnessUnfitness(List<Piesa> programPieces)
        {
            int sum = 0;
            int noPieces = 0;
            for (int i = 0; i < programPieces.Count; i++)
            {
                noPieces += programPieces[i].NumarExemplare;
            }
            for (int i = 0; i < noPieces; i++)
            {
                if (Cromozom[2, i] == 1)
                    sum += programPieces[Cromozom[0, i]].Valoare;
            }
            //calcul unfitness
            int maximumUnfitness = 0;

            for (int i = 0; i < noPieces; i++)
            {
                if (Cromozom[2, i] == 0)
                    continue;
                for (int j = i+1; j < noPieces; j++)
                {
                    if (Cromozom[2, j] == 0)
                        continue;
                    int valueX = Cromozom[3, i] - Cromozom[3, j] - (programPieces[Cromozom[0, i]].Lungime
                        + programPieces[Cromozom[0, j]].Lungime) / 2;
                    int valueY = Cromozom[4, i] - Cromozom[4, j] - (programPieces[Cromozom[0, i]].Latime 
                        + programPieces[Cromozom[0, j]].Latime) / 2;
                    if (valueX < 0 && valueY < 0)
                        maximumUnfitness = Math.Abs(valueX) * Math.Abs(valueY);
                }
            }

            return new Tuple<int,int>(sum,maximumUnfitness);
        }
        public Individ CrossOver(Individ individ)
        {
            int[,] cromozomChild= new int[5, Cromozom.Length/5];
            Random random = new Random();
            for (int i = 0; i < Cromozom.Length/5; i++)
            {
                int currentRow = random.Next(0, 2);
                if(currentRow == 0)
                {
                    cromozomChild[0, i] = Cromozom[0, i];
                    cromozomChild[1, i] = Cromozom[1, i];
                    cromozomChild[2, i] = Cromozom[2, i];
                    cromozomChild[3, i] = Cromozom[3, i];
                    cromozomChild[4, i] = Cromozom[4, i];
                }
                else
                {
                    cromozomChild[0, i] = individ.Cromozom[0, i];
                    cromozomChild[1, i] = individ.Cromozom[1, i];
                    cromozomChild[2, i] = individ.Cromozom[2, i];
                    cromozomChild[3, i] = individ.Cromozom[3, i];
                    cromozomChild[4, i] = individ.Cromozom[4, i];
                }
            }
            return new Individ(cromozomChild);
        }

        public Individ Mutatie()
        {
            Random random = new Random();
            double mutationValue = random.NextDouble();
            if (mutationValue >= 0.55)
            {
                int rowToChange = random.Next(0, Cromozom.Length / 5);
                Cromozom[2, rowToChange] = 0;
            }
            return this;
        }

        public Individ Improvement(List<Piesa> programPieces, Placa cuttingSpace)
        {
            for (int i = 0; i < Cromozom.Length/5; i++)
            {
                if(Cromozom[2,i] == 1)
                {
                    //incercare mutare stanga
                    if (Cromozom[3,i] -1 >= programPieces[Cromozom[0,i]].Lungime/2)
                    {
                        int oldUnfitness = FitnessUnfitness.Item2;
                        Cromozom[3, i] -= 1;
                        CalculateFitnessUnfitness(programPieces);
                        if(oldUnfitness < FitnessUnfitness.Item2)
                        {
                            Cromozom[3, i] += 1;
                            FitnessUnfitness =new Tuple<int,int>(FitnessUnfitness.Item1,oldUnfitness);
                        }
                    }

                    //incercare mutare jos
                    if (Cromozom[4, i] - 1 >= programPieces[Cromozom[0, i]].Latime / 2)
                    {
                        int oldUnfitness = FitnessUnfitness.Item2;
                        Cromozom[4, i] -= 1;
                        CalculateFitnessUnfitness(programPieces);
                        if (oldUnfitness < FitnessUnfitness.Item2)
                        {
                            Cromozom[4, i] += 1;
                            FitnessUnfitness = new Tuple<int, int>(FitnessUnfitness.Item1, oldUnfitness);
                        }
                    }

                    //incercare mutare dreapta
                    if (Cromozom[3, i] + 1 <= cuttingSpace.Lungime - programPieces[0].Lungime / 2)
                    {
                        int oldUnfitness = FitnessUnfitness.Item2;
                        Cromozom[3, i] += 1;
                        CalculateFitnessUnfitness(programPieces);
                        if (oldUnfitness <= FitnessUnfitness.Item2)
                        {
                            Cromozom[3, i] -= 1;
                            FitnessUnfitness = new Tuple<int, int>(FitnessUnfitness.Item1, oldUnfitness);
                        }
                    }

                    //incercare mutare sus
                    if (Cromozom[4, i] + 1 <= cuttingSpace.Latime - programPieces[0].Latime / 2)
                    {
                        int oldUnfitness = FitnessUnfitness.Item2;
                        Cromozom[4, i] += 1;
                        CalculateFitnessUnfitness(programPieces);
                        if (oldUnfitness <= FitnessUnfitness.Item2)
                        {
                            Cromozom[4, i] -= 1;
                            FitnessUnfitness = new Tuple<int, int>(FitnessUnfitness.Item1, oldUnfitness);
                        }
                    }
                }
            }
            return this;
        }
    }
}
