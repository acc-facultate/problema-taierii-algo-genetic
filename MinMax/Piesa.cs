﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taiere2D
{
    class Piesa
    {
        public int Lungime { get; set; }
        public int Latime { get; set; }
        public int Valoare { get; set; }
        public int NumarExemplare { get; set; }
        public Piesa(int lungimePiesa, int latimePiesa, int valoarePiesa, int nrExemplarePiesa)
        {
            Lungime = lungimePiesa;
            Latime = latimePiesa;
            Valoare = valoarePiesa;
            NumarExemplare = nrExemplarePiesa;
        }
    }
}
