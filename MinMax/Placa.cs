﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taiere2D
{
    class Placa
    {
        public int Lungime { get; set; }
        public int Latime { get; set; }
        public Placa(int lungimePlaca, int latimePlaca)
        {
            Lungime = lungimePlaca;
            Latime = latimePlaca;
        }
    }
}
