﻿using System;

namespace Taiere2D
{
    class Program
    {
        static void Main(string[] args)
        {
            Algorithm algorithm = new Algorithm();
            algorithm.ReadInput();
            algorithm.StartSimulation(30,7500);
        }
    }
}
