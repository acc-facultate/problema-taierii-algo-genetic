﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Taiere2D
{
    class Algorithm
    {
        private List<Piesa> programPieces = new List<Piesa>();
        private Placa cuttingSpace;
        private Tuple<int, int> BestInGeneration { get; set; } = new Tuple<int, int>(Int32.MinValue, Int32.MaxValue);

        public void StartSimulation(int populationSize, int noExecutions)
        {
            Generatie generatie = new Generatie(populationSize, programPieces, cuttingSpace);
            int counterGeneration = 1;
            while( counterGeneration < noExecutions)
            {
                if(generatie.BestInGeneration != null)
                {
                    if (generatie.BestInGeneration.FitnessUnfitness.Item1 > BestInGeneration.Item1)
                        BestInGeneration = generatie.BestInGeneration.FitnessUnfitness;
                }
                Generatie succesor = new Generatie(generatie.NewGeneration, programPieces, cuttingSpace);
                generatie = succesor;
                counterGeneration++;
            }
            Console.WriteLine("Cel mai bun rezultat in urma simularii este: " + BestInGeneration);
        }

        public void ReadInput()
        {
            using (var sr = new StreamReader("input.txt"))
            { 
                int lungimePlaca = Int32.Parse(sr.ReadLine());
                int latimePlaca = Int32.Parse(sr.ReadLine());
                cuttingSpace = new Placa(lungimePlaca, latimePlaca);

                int nrPiese = Int32.Parse(sr.ReadLine());

                for (int i = 0; i < nrPiese; i++)
                {
                    int lungimePiesa = Int32.Parse(sr.ReadLine());

                    int latimePiesa = Int32.Parse(sr.ReadLine());

                    int valoarePiesa = Int32.Parse(sr.ReadLine());

                    int nrExemplarePiesa = Int32.Parse(sr.ReadLine());

                    Piesa piesaNoua = new Piesa(lungimePiesa, latimePiesa, valoarePiesa, nrExemplarePiesa);
                    programPieces.Add(piesaNoua);
                }
            }
        }
    }
}
