﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taiere2D
{
    class Generatie
    {
        public List<Individ> Population { get; set; }
        public List<Individ> NewGeneration { get; set; }
        public Individ BestInGeneration { get; set; }

        public Generatie(int populationSize, List<Piesa> programPieces, Placa cuttingSpace)
        {
            Population = new List<Individ>();
            NewGeneration = new List<Individ>();
            RandomlyGeneratePopulation(populationSize, programPieces, cuttingSpace);
            SelectionAndCrossOverAndMutation(programPieces, cuttingSpace);
            DetermineBestInGeneration();
        }
        public Generatie(List<Individ> population, List<Piesa> programPieces, Placa cuttingSpace)
        {
            Population = population;
            NewGeneration = new List<Individ>();
            SelectionAndCrossOverAndMutation(programPieces,cuttingSpace);
            DetermineBestInGeneration();
        }

        private void RandomlyGeneratePopulation(int populationSize, List<Piesa> programPieces, Placa cuttingSpace)
        {
            for (int i = 0; i < populationSize; i++)
            {
                Individ auxIndivid = new Individ(programPieces,cuttingSpace);
                Population.Add(auxIndivid);
            }
        }

        public void SelectionAndCrossOverAndMutation(List<Piesa> programPieces, Placa cuttingSpace)
        {
            Random random = new Random();

            //Selectie
            int firstIndividualSelected = random.Next(0, Population.Count);
            int secondIndividualSelected = random.Next(0, Population.Count);
            while(secondIndividualSelected == firstIndividualSelected)
                secondIndividualSelected = random.Next(0, Population.Count);

            //Crossover
            Individ child = Population[firstIndividualSelected].CrossOver(Population[secondIndividualSelected]);

            //Mutatie
            child = child.Mutatie();
            child.FitnessUnfitness = child.CalculateFitnessUnfitness(programPieces);

            //Improvement
            child = child.Improvement(programPieces, cuttingSpace);

            //Check existenta in populatie
            int cutPiecesChild = 0;
            bool alreadyExists = false;
            for (int i = 0; i < child.Cromozom.Length/5; i++)
            {
                if (child.Cromozom[2, i] == 1)
                    cutPiecesChild++;
            }

            for (int i = 0; i < Population.Count; i++)
            {
                int piecesFound = 0;
                for (int j = 0; j < child.Cromozom.Length/5; j++)
                {
                    if(child.Cromozom[2,j] ==1)
                    {
                        for (int k = 0; k < Population[i].Cromozom.Length/5; k++)
                        {
                            if(Population[i].Cromozom[2,k] == 1)
                                if(child.Cromozom[3, j] == Population[i].Cromozom[3,k] && child.Cromozom[4,j] == Population[i].Cromozom[4, k])
                                {
                                    piecesFound++;
                                    break;
                                }
                        }
                    }
                }
                if(piecesFound == cutPiecesChild)
                {
                    alreadyExists = true;
                }
            }

            //Replacement
            if (alreadyExists == false)
            {

                List<int> indexesGroup1 = new List<int>();
                List<int> indexesGroup2 = new List<int>();
                List<int> indexesGroup3 = new List<int>();
                List<int> indexesGroup4 = new List<int>();

                for (int index = 0; index < Population.Count; index++)
                {
                    //group 1
                    if (child.FitnessUnfitness.Item1 > Population[index].FitnessUnfitness.Item1 && child.FitnessUnfitness.Item2 < Population[index].FitnessUnfitness.Item2)
                    {
                        indexesGroup1.Add(index);
                        continue;
                    }

                    //group 4
                    if (child.FitnessUnfitness.Item1 < Population[index].FitnessUnfitness.Item1 && child.FitnessUnfitness.Item2 > Population[index].FitnessUnfitness.Item2)
                    {
                        indexesGroup4.Add(index);
                        continue;
                    }

                    //group 2
                    if (child.FitnessUnfitness.Item2 < Population[index].FitnessUnfitness.Item2)
                    {
                        indexesGroup2.Add(index);
                        continue;
                    }

                    //group 3
                    indexesGroup3.Add(index);

                }

                int indexIndividualToDelete;

                if (indexesGroup1.Count > 0)
                {
                    int indexGroup = random.Next(0, indexesGroup1.Count);
                    indexIndividualToDelete = indexesGroup1[indexGroup];
                }
                else if (indexesGroup2.Count > 0)
                {
                    int indexGroup = random.Next(0, indexesGroup2.Count);
                    indexIndividualToDelete = indexesGroup2[indexGroup];
                }
                else if (indexesGroup3.Count > 0)
                {
                    int indexGroup = random.Next(0, indexesGroup3.Count);
                    indexIndividualToDelete = indexesGroup3[indexGroup];
                }
                else
                {
                    int indexGroup = random.Next(0, indexesGroup4.Count);
                    indexIndividualToDelete = indexesGroup4[indexGroup];
                }
                Population.RemoveAt(indexIndividualToDelete);

                Population.Add(child);
            }

            NewGeneration = Population;
        }

        public void DetermineBestInGeneration()
        {
            //int minFitness = Int32.MaxValue;
            //int maxFitness = Int32.MinValue;
            //int minUnfitness = Int32.MaxValue;
            //int maxUnfitness = Int32.MinValue;
            //for (int i = 0; i < NewGeneration.Count; i++)
            //{
            //    if (minFitness > NewGeneration[i].FitnessUnfitness.Item1)
            //        minFitness = NewGeneration[i].FitnessUnfitness.Item1;

            //    if (maxFitness < NewGeneration[i].FitnessUnfitness.Item1)
            //        maxFitness = NewGeneration[i].FitnessUnfitness.Item1;

            //    if (minUnfitness > NewGeneration[i].FitnessUnfitness.Item2)
            //        minUnfitness = NewGeneration[i].FitnessUnfitness.Item2;

            //    if (maxUnfitness < NewGeneration[i].FitnessUnfitness.Item2)
            //        minUnfitness = NewGeneration[i].FitnessUnfitness.Item2;
            //}

            //List<int> fitnessScaled = new List<int>();
            //List<int> unfitnessScaled = new List<int>();
            //for (int i = 0; i < NewGeneration.Count; i++)
            //{
            //    int currentFitnessScaled = (NewGeneration[i].FitnessUnfitness.Item1 - minFitness) / (maxFitness - minFitness);
            //    int currentUnfitnessScaled = (NewGeneration[i].FitnessUnfitness.Item2 - minUnfitness) / (maxUnfitness - minUnfitness);
            //    fitnessScaled.Add(currentFitnessScaled);
            //    unfitnessScaled.Add(currentUnfitnessScaled);
            //}


            long bestFitness = 0;
            for (int i = 0; i < NewGeneration.Count; i++)
            {
                if(NewGeneration[i].FitnessUnfitness.Item2 ==0)
                    if(NewGeneration[i].FitnessUnfitness.Item1 > bestFitness)
                    {
                        bestFitness = NewGeneration[i].FitnessUnfitness.Item1;
                        BestInGeneration = NewGeneration[i];
                    }
            }
        }
    }
}
