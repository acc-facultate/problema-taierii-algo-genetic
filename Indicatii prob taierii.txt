individ = pozitionare a pieselor pe material

Ex:
tip 		1 1 1   2 2
exemplar 	1 2 3   1 2
Zip		1 0 0   1 1 (ne spune daca copia piesei i este taiata sau nu)
Xip		15          (desi pe desen ar veni 30  
Yip		8	    (desi pe desen ar veni 15)

fitness = valoarea pieselor pe care le taiem
conditia ca ele sa nu se suprapuna : se gaseste in o functie care se cheama unfitness
ca sa incapa pe panza, x > L/2 si x < L piesa - L/20

xIp apartine lui [Li/2, L0 - Li/2]
xIp are valori intre [0,1] -> [0; L0-Li] -> [Li/2,L0-Li/2]

yIp are valori intre [0,1]

mutatia in prob asta, daca un copil e selectat pt mutatie, atunci o piesa din copil are setat Zjp la 0;


Prob suprapunerii se masoara prin unfitness, intr-o situatie ideala unfitness e 0.
Se iau fiecare 2 piese, si se face diferenta intre x-uri si alfa, si y-uri si alfa, si se ia in considerare
doar daca cele 2 piese se taie.

la inceput ar merge cu 2-3 piese, ca sa vedem daca merge algoritmul.

pt unfitness calculam lungime piesa 1 - lungime piesa 2 (1,2 se face prin mers ca la prosti prin 2 foruri)
(lungime piesa 1 - lungime piesa 2) - medie aritmetica a lungimii pieselor care se scad in prima paranteza.